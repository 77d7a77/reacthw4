import React from 'react';

function Button({ classNameElem, handlClick, btnText }) {
  return (
    <button onClick={handlClick} className={classNameElem}>
      {btnText}
    </button>
  );
}

export default Button;