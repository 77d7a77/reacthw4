import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { AiOutlineDelete } from 'react-icons/ai';
import { useSelector } from 'react-redux';
const NavComponentStyle = styled.nav`
  background-color: rgba(200, 0, 255, 1);
  .nav__list {
    display: flex;
    justify-content: space-around;
    list-style: none;
  }
  .nav__item {
    text-align: center;
    padding: 14px 16px;
  }
  a {
    color: black;
  }
`;


function NavComponent() {
  const countFavoritesCards = useSelector((state)=>
  state.cards.all.filter(({favorites})=>favorites))
  const countBuyCards = useSelector((state)=>
   state.cards.all.filter(({buy})=>buy)
   )
  return (
    <NavComponentStyle className='page__nav'>
      <ul className='nav__list'>
        <li className='nav__item'>
          <Link to='/'>GAMES</Link>
        </li>
        <li className='nav__item'>
          <Link to='/favorites'>FAVORITES ({countFavoritesCards.length})</Link>
        </li>
        <li className='nav__item'>
        <Link to='/buy'><AiOutlineDelete/>({countBuyCards.length})</Link>
        </li>
      </ul>
    </NavComponentStyle>
  );
}

export default NavComponent;